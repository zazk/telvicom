# Create your views here.
from django.shortcuts import get_object_or_404, render_to_response as render
from django.template import RequestContext 
import os

# Nueva Vista
def url_template(request, template=''):
    home = -False
    print "#####################"
    print os.listdir('templates/website')
    print "#####################"
    templates = os.listdir('templates/website')
    base = set(['header.html', 'base.html', 'footer.html','submenu.html',
        'pagination.html','index.html','404.html']) # the subset of A 
    templates = [a for a in templates if a not in base]

    if not template:
        home = True
        return render('website/index.html', locals(),
            context_instance=RequestContext(request))
    return render('website/'+template+ '.html', locals(),
        context_instance=RequestContext(request))