from django.conf.urls.defaults import patterns, include, url

from django.conf import settings
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'telvicom.views.home', name='home'),
    # url(r'^telvicom/', include('telvicom.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),    
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}), 
    
    # Front-end  
    url(r'^$',
    'apps.website.views.url_template', name='url_template'),
    url(r'^(?P<template>(\w|-)*).html$', 
    'apps.website.views.url_template', name='url_template'),

)
