$(function(){
  $('.slider').cycle({
    after:function(currSlideElement, nextSlideElement, options){
      $('.slider-controllers a').removeClass('active');
      $('.slider-controllers a').eq(options.currSlide).addClass('active');
      console.log(options.currSlide);
    }
  });
   $('.slider-controllers a').each(function(i,e){
      $(e).click(function(ev){
        ev.preventDefault();
        $('.slider').cycle(i);
      })
   });
  $('.nav  >  ul > li').hover(function(){$(this).addClass('active')},function(){$(this).removeClass('active')});
    
  $('[placeholder]').focus(function() {
    var input = $(this);
    if (input.val() == input.attr('placeholder')) {
      input.val('');
      input.removeClass('placeholder');
    }
  }).blur(function() {
    var input = $(this);
    if (input.val() == '' || input.val() == input.attr('placeholder')) {
      input.addClass('placeholder');
      input.val(input.attr('placeholder'));
    }
  }).blur();
});